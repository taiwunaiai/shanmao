// pages/next/next.js
Page({

  /**
   * 页面的初始数据
   */
    data: {
        region: ['国家', '地区', '中国'],
        customItem: '全部'
    },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },
  goClose: function () {
      wx.showModal({
          title: '确认手机号码',
          content: '我们将发送验证码到14785296370',
          //showCancel: false,   //是否显示取消按钮
          success: function (res) {
              if (res.confirm) {
                  // 点击了确认
              } else {
                  // 点击了取消
              }
          }
      })

  },
  bindRegionChange: function (e) {
      // console.log('picker发送选择改变，携带值为', e.detail.value)
      this.setData({
          region: e.detail.value
      })
  },
  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})
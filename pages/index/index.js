//index.js
//获取应用实例
var app = getApp()

Page({
  data: {
    goods:[],
    imgUrls: [
      'http://img02.tooopen.com/images/20150928/tooopen_sy_143912755726.jpg',
      'http://img06.tooopen.com/images/20160818/tooopen_sy_175866434296.jpg',
      'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg'
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    indicatorActiveColor:"#ff3600",
    spac:"<"
  },
  //跳转详情
  toDetailsTap: function (e) {
    wx.navigateTo({
      url: "/pages/goods-details/index?id=" + e.currentTarget.dataset.id
    })
  },
  changeIndicatorDots: function (e) {
    this.setData({
      indicatorDots: !this.data.indicatorDots
    })
  },
  changeAutoplay: function (e) {
    this.setData({
      autoplay: !this.data.autoplay
    })
  },
  intervalChange: function (e) {
    this.setData({
      interval: e.detail.value
    })
  },
  durationChange: function (e) {
    this.setData({
      duration: e.detail.value
    })
  },
 onLoad:function(){
   var that = this
   wx.request({
     url: 'https://api.it120.cc/' + app.globalData.subDomain + '/shop/goods/category/all',
     success: function (res) {
       var categories = [{ id: 0, name: "全部" }];
       if (res.data.code == 0) {
         for (var i = 0; i < res.data.data.length; i++) {
           categories.push(res.data.data[i]);
         }
       }
       that.setData({
         categories: categories,
         activeCategoryId: 0
       });
       that.getGoodsList(0);
     }
   })
 },
 getGoodsList: function (categoryId) {
   if (categoryId == 0) {
     categoryId = "";
   }
   console.log(categoryId)
   var that = this;
   wx.request({
     url: 'https://api.it120.cc/' + app.globalData.subDomain + '/shop/goods/list',
     data: {
       categoryId: categoryId,
       nameLike: ""
     },
     success: function (res) {
       that.setData({
         goods: [],
         loadingMoreHidden: true
       });
       var goods = [];
       if (res.data.code != 0 || res.data.data.length == 0) {
         that.setData({
           loadingMoreHidden: false,
         });
         return;
       }
       console.log(res.data.data.length)
       for (var i = 0; i < res.data.data.length; i++) {
         goods.push(res.data.data[i]);
       }
       
       that.setData({
         goods: goods,
       });
     }
   })
 }
})
